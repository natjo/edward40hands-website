const DB = require('../db');


exports.uploadGame = function(req, res){
    let unixDate = Math.round((new Date()).getTime() / 1000);
    let name1 = req.body.name1;
    let name2 = req.body.name2;
    let time1 = req.body.time1;
    let time2 = req.body.time2;
    let extraBeers1 = req.body.extraBeers1;
    let extraBeers2 = req.body.extraBeers2;
    console.log("New game uploaded");
    DB.newGame(unixDate, name1, name2, time1, time2, extraBeers1, extraBeers2);
}
