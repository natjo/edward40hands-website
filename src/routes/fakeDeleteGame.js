const DB = require('../db');


exports.fakeDeleteGame = function(req, res){
    let unixDate = parseInt(req.body.unixDate);
    console.log(`Fake deleting game at unixDate ${unixDate}`);
    DB.fakeDeleteGame(unixDate);
}
