const DB = require('../db');

exports.getIcon = function(req, res){
    let name = req.body.playerName;
    console.log(`Requesting icon of user ${name}`);
    DB.getIcon(name, res);
}
