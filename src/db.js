var MongoClient = require('mongodb').MongoClient;

const DB_URL = "mongodb://localhost:27017";
const DB_NAME = "edward40handsdb";

// Names of collections
const COL_PLAYERS = "players";
const COL_GAMES = "games";
const COL_DEL_GAMES = "deletedGames";

// Create database
exports.createDatabases = function(playerNames){
    console.log('Create database');
    callFunctionOnDB((db, dbc) => {

        db.createCollection(COL_PLAYERS, {strict:true}, function(err, collection){});
        db.createCollection(COL_GAMES, {strict:true}, function(err, collection){});
        db.createCollection(COL_DEL_GAMES, {strict:true}, function(err, collection){});

        let playerDocs = []
        playerNames.forEach(name => {
            playerDocs.push(generatePlayerDoc(name));
        });

        db.collection(COL_PLAYERS).insertMany(playerDocs, {w:1}, function(err, result) {dbc.close()});
    });
}

// Remove user
exports.deleteUser = function(name){
    console.log(`Delete user with name ${name}`);
    callFunctionOnDB((db, dbc) => {
        let query = {"name":name};
        db.collection(COL_PLAYERS).deleteOne(query, function(err, obj){dbc.close()});
    });
}

// Add game
exports.newGame = function(unixDate, name1, name2, time1, time2, extraBeers1, extraBeers2){
    console.log(`Add new game: at: ${unixDate}, names: ${name1}:${name2}, times: ${time1}:${time2}, beers: ${extraBeers1}:${extraBeers2}`);
    callFunctionOnDB((db, dbc) => {
        let gameDoc = generateGameDoc(unixDate, name1, name2, time1, time2, extraBeers1, extraBeers2);
        db.collection(COL_GAMES).insertOne(gameDoc, {w:1}, function(err, result) {dbc.close()});
    });
}

// Remove game
exports.deleteGame = function(unixDate){
    console.log(`Delete game at ${unixDate}`);
    callFunctionOnDB((db, dbc) => {
        let query = {"unixDate": unixDate};
        db.collection(COL_GAMES).deleteOne(query, function(err, obj){dbc.close()});
    });
}

// Fake-delete game, actually just move it to the collection with deleted games
exports.fakeDeleteGame = function(unixDate){
    console.log(`Move game to fake deleted games: at: ${unixDate}`);
    // Find game
    callFunctionOnDB((db, dbc) => {
        let query = {"unixDate": unixDate};
        projection = {_id:0};  // All but _id
        db.collection(COL_GAMES).find(query).project(projection).toArray(function(err, result){

            // Create game on the deleted collection
            callFunctionOnDB((db, dbc) => {
                let gameDoc = result[0];
                db.collection(COL_DEL_GAMES).insertOne(gameDoc, {w:1}, function(err, result) {dbc.close()});
            });

            // Delete original game
            exports.deleteGame(unixDate);

            dbc.close();
        });
    });
}


// GET VALUES
exports.getAllUserInfo = function (res){
    callFunctionOnDB((db, dbc) => {
        query = {};
        projection = {_id:0};  // All but _id
        db.collection(COL_PLAYERS).find(query).project(projection).toArray(function(err, result){
            dbc.close();
            res.send(result);
        });
    });
}

exports.getAllGames = function (res){
    callFunctionOnDB((db, dbc) => {
        query = {};
        projection = {_id:0};  // All but _id
        db.collection(COL_GAMES).find(query).project(projection).toArray(function(err, result){
            dbc.close();
            res.send(result);
        });
    });
}

exports.getIcon = function(name, res){
    return getSingleField("iconURL", COL_PLAYERS,  name, res);
}


function getSingleField(field, col, name, res){
    callFunctionOnDB((db, dbc) => {
        let query = { name: name };

        db.collection(col).find(query).toArray(function(err, result){
            dbc.close()
            res.send(result[0][field]);
        });
    });
}


// UPDATES
function updateSingleField(field, col, name, newValue){
    console.log(`Update ${field} of ${name} to ${newValue}`);
    callFunctionOnDB((db, dbc) => {
        let query = {"name":name};
        let update = {};
        update[field] = newValue;
        let newValues = { $set:update };
        db.collection(col).updateOne(query, newValues, function(err, obj){dbc.close()});
    });
}

exports.updateIconURL = function(name, newVal){
    updateSingleField("iconURL",COL_PLAYERS, name, newVal);
}


// Wrapper that initializes connection to database
// func must have (db, dbc) as parameters
function callFunctionOnDB(func){
    MongoClient.connect(DB_URL, {useNewUrlParser:true}, function(err, dbc) {
        if(err){
	    console.log(err);
            console.log("Couldn't connect to database");
            return;
        }
        db = dbc.db(DB_NAME);
        func(db, dbc);
    });
}

function generatePlayerDoc(name){
    return {'name':name, 'iconURL':'none.jpg'};
}

function generateGameDoc(unixDate, name1, name2, time1, time2, extraBeers1, extraBeers2){
    return {'unixDate': unixDate, 'name1':name1, 'name2':name2, 'time1':time1, 'time2':time2, 'extraBeers1':extraBeers1, 'extraBeers2':extraBeers2};
}
