var express = require('express');
var multer = require('multer');
var nunjucks = require('nunjucks');
var path = require('path');
var favicon = require('serve-favicon');
var DB = require('./src/db');


const PORT = process.env.PORT || 8080;
var app = express();



// Server settings
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use(favicon(path.join(__dirname, 'public','icons','favicon.ico')));

var env = nunjucks.configure(['public/html/'], {
    autoescape: true,
    express: app
});

// Init mutter
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/images') //Destination folder
  },
  filename: function (req, file, cb) {
    var fileObj = {
        "image/png": ".png",
        "image/jpeg": ".jpeg",
        "image/jpg": ".jpg"
    };
    // Update database with new image name
    DB.updateIconURL(req.body.playerName, req.body.playerName + fileObj[file.mimetype]);

    cb(null, req.body.playerName + fileObj[file.mimetype]) //File name after saving
  }
})

var upload = multer({ storage: storage })


// Routes for REST
app.get('/getTableInfo', require('./src/routes/getTableInfo').getTableInfo);
app.get('/getAllGames', require('./src/routes/getAllGames').getAllGames);
app.post('/getIcon', require('./src/routes/getIcon').getIcon);
app.post('/fakeDeleteGame', require('./src/routes/fakeDeleteGame').fakeDeleteGame);
app.post('/uploadGame', require('./src/routes/uploadGame').uploadGame);


// Upload image
app.post('/upload-image', upload.single('image'), function(req, res, next) {});


// Routes to user pages
app.get('/overview', function(req, res){
    res.render('overview.html', {page_title: 'Übersicht'});
});

app.get('/', function(req, res){
    res.render('overview.html', {page_title: 'Übersicht'});
});

['jonas', 'simon', 'matthias', 'benni', 'bene', 'tobi'].forEach((playerName) => {
    app.get(`/${playerName}`, function(req, res){
        res.render('player.html', {page_title: 'Spielerdetails', name: playerName});
    });
});

app.get('/rules', function(req, res){
    res.render('rules.html', {page_title: 'Regeln'});
});

app.get('/trophy', function(req, res){
    res.render('trophy.html', {page_title: 'Der Pokal'});
});

app.get('/game-history', function(req, res){
    res.render('game_history.html', {page_title: 'Spielhistorie'});
});

app.get('/game', function(req, res){
    res.render('game.html', {page_title: 'Neue Runde'});
});

app.get('/impressum', function(req, res){
    res.render('impressum.html', {page_title: 'Impressum'});
});

app.use(express.static(__dirname + '/public', {'extensions': ['jpg', 'png', 'jpeg']}));

app.get('/', function(req, res){
    res.render('index.html', {page_title: 'Fake page'});
});

app.use(function(req, res){
    res.sendStatus(404);
});


// Start server
app.listen(PORT, () => console.log('Listening on port 8080'));

