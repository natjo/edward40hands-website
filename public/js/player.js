document.addEventListener('DOMContentLoaded', function() {

    //TODO Get name from hidden element
    let name = $('#playerIcon').attr('alt');
    setPlayerIcon(name);
    loadGames(name);
    loadPlayerStats(name);

    // Handle picture upload
    $('#formUpdateImage').on('submit', function(){
        //Check if a valid file was set
        var form = $('#formUpdateImage')[0];
        var formData = new FormData(form);
        if($('#formImageAddress')[0].value == ""){
            alert('Du hast kein Bild ausgewählt. Klicke dazu einfach auf "Date wählen".');
        }else{
            alert("Lade die Seite neu, um dein neues Bild zu sehen.");
        }
    $.ajax({
        url: 'upload-image',
        data: formData,
        type: 'POST',
        contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
        processData: false, // NEEDED, DON'T OMIT THIS
        success: function(result){
            console.log("update image");
            setPlayerIcon();
        },
        // error: function(error){
        //     alert('Etwas lief schief beim hochladen');
        // }
    });
        return false;
    });
}, false);

function setPlayerIcon(name){
    $.post('/getIcon', {'playerName': name}, function(data){
        $("#playerIcon").attr("src",`/images/${data}`);
    });
}

function loadGames(name){
    $.get('/getAllGames', function(dataGames){
        // First sort data, first by date
        dataGames.sort(function(a,b) {
            return a.unixDate > b.unixDate;
        });

        // Iterate over games and add table row, iterate only if the name is there
        dataGames.filter(doc => doc.name1 == name || doc.name2 == name).forEach(doc => {
            // Check who won, print accordingly
	    var winner = doc.name1;
	    if(parseInt(doc.time1) > parseInt(doc.time2)){
	        winner = doc.name2;
	    }
            if(doc.name1 == name){
                // name1 is the player
                var nameAgainst = doc.name2;
                var timeAgainst = doc.time2;
                var timeOwn = doc.time1;
                var extraBeersAgainst = doc.extraBeers2;
                var extraBeersOwn = doc.extraBeers1;
            }else{
                // name2 is the player
                var nameAgainst = doc.name1;
                var timeAgainst = doc.time1;
                var timeOwn = doc.time2;
                var extraBeersAgainst = doc.extraBeers1;
                var extraBeersOwn = doc.extraBeers2;
            }
            $('#tableGames tbody:last-child').append(
                generateRowHTML(
                    capitalize(nameAgainst),
                    capitalize(winner),
                    sec2str(timeAgainst),
                    sec2str(timeOwn),
                    extraBeersAgainst,
                    extraBeersOwn,
                    unixDate2str(doc.unixDate)));
        });
    });
}

function loadPlayerStats(name){
    $.get('/getAllGames', function(dataGames){
        let points = getPoints(dataGames, name);
        let games = getGames(dataGames, name);
        let extraBeers = getTotalExtraBeer(dataGames, name);
        let averageTime = 0;
        if(games != 0){
            averageTime = getTotalTime(dataGames, name)/games;
        }

        $(`#points`).text(points);
        $(`#totalGames`).text(games);
        $(`#extraBeers`).text(extraBeers);
        $(`#averageTime`).text(sec2str(averageTime));
    });
}

function getPoints(data, name){
    let wins = data.filter(d => (d.name1 == name  && parseInt(d.time1) < parseInt(d.time2)) || (d.name2 == name  && parseInt(d.time2) < parseInt(d.time1))).length;
    let losses = data.filter(d => (d.name1 == name  && parseInt(d.time1) > parseInt(d.time2)) || (d.name2 == name  && parseInt(d.time2) > parseInt(d.time1))).length;

    return wins-losses;
}

function getGames(data, name){
    return data.filter(d => d.name1 == name || d.name2 == name).length;
}

function getTotalExtraBeer(data, name){
    let sum = 0;
    data.forEach(d =>{
        if(d.name1 == name){
            sum += parseInt(d.extraBeers1);
        }else{
            if(d.name2 == name){
                sum += parseInt(d.extraBeers2);
            }
        }
    });
    return sum;
}

function getTotalTime(data, name){
    let sum = 0;
    data.forEach(d =>{
        if(d.name1 == name){
            sum += parseInt(d.time1);
        }else{
            if(d.name2 == name){
                sum += parseInt(d.time2);
            }
        }
    });
    return sum;
}

function generateRowHTML(nameAgainstStr, winnerStr, timeAgainst, timeOwn, extraBeersAgainst, extraBeersOwn, dateStr){
    let tableRow = `<tr class="text-center"><td scope="row">${nameAgainstStr}</td><td>${winnerStr}</td><td>${timeAgainst} - ${timeOwn}</td><td>${extraBeersAgainst} - ${extraBeersOwn}</td><td>${dateStr}</td></tr>`
    return tableRow;
}

function unixDate2str(unixDate){
    // Create a new JavaScript Date object based on the timestamp
    // multiplied by 1000 so that the argument is in milliseconds, not seconds.
    var date = new Date(unixDate*1000);
    // var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = date.getFullYear();
    // var month = months[date.getMonth()];
    var month = date.getMonth() + 1;
    var day = date.getDate();
    // var hours = date.getHours();
    // var minutes = "0" + date.getMinutes();
    // var seconds = "0" + date.getSeconds();
    // return hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    return day + '.' + month + "." + year.toString().substr(-2);
}

function sec2str(seconds) {
    var sec_num = parseInt(seconds, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours+':'+minutes+':'+seconds;
};

function capitalize(lower){
    return lower.charAt(0).toUpperCase() + lower.substr(1);
}
