// Load data during first call
document.addEventListener('DOMContentLoaded', function() {
    onLoad();
}, false);

function onLoad(){
    loadUserData();
}

function generateRowHTML(image, name, points, numberOfGames, overallTime, averageTime){
    let imgSize = 96;
    let imgElement = `<img src="/images/${image}" alt="image_of_${name}" height="${imgSize}" width="${imgSize}">`;
    let tableRow = `<tr class="text-center"><th>${imgElement}</th><td scope="row">${name}</td><td>${points}</td><td>${numberOfGames}</td><td>${overallTime}</td><td>${averageTime}</td></tr>`
    return tableRow;
}

function loadUserData(){
    let store = {};
        $.when(
            $.get('/getTableInfo', function(dataPlayers){
                store.dataPlayers = dataPlayers;
            }),
            $.get('getAllGames', function(dataGames){
                store.dataGames = dataGames;
            })
        ).then(function() {
            // Iterate over player and update info
            store.dataPlayers.forEach(doc => {
                doc.points = getPoints(store.dataGames, doc.name);
                doc.games = getGames(store.dataGames, doc.name);
                doc.totalTime = getTotalTime(store.dataGames, doc.name);
                doc.averageTime = 0;
                if(doc.games != 0){
                    doc.averageTime = doc.totalTime/doc.games;
                }
            });

            // Sort dataPlayers, first by points, then by time
            store.dataPlayers.sort(function(a,b) {
                return a.points < b.points ? 1 : a.points > b.points ? -1 : a.averageTime > b.averageTime ? 1 : a.averageTime < b.averageTime ? -1 : 0;
            });

            // Print the table
            store.dataPlayers.forEach(doc => {
                $('#tableLeague tbody:last-child').append(
                    generateRowHTML(doc.iconURL, capitalize(doc.name), doc.points, doc.games, sec2str(doc.totalTime), sec2str(doc.averageTime)));
        });
    });
}

function getPoints(data, name){
    let wins = data.filter(d => (d.name1 == name  && parseInt(d.time1) < parseInt(d.time2)) || (d.name2 == name  && parseInt(d.time2) < parseInt(d.time1))).length;
    let losses = data.filter(d => (d.name1 == name  && parseInt(d.time1) > parseInt(d.time2)) || (d.name2 == name  && parseInt(d.time2) > parseInt(d.time1))).length;

    return wins-losses;
}

function getGames(data, name){
    return data.filter(d => d.name1 == name || d.name2 == name).length;
}

function getTotalTime(data, name){
    let sum = 0;
    data.forEach(d =>{
        if(d.name1 == name){
            sum += parseInt(d.time1);
        }else{
            if(d.name2 == name){
                sum += parseInt(d.time2);
            }
        }
    });
    return sum;
}

function sec2str(seconds) {
    var sec_num = parseInt(seconds, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours+':'+minutes+':'+seconds;
};

function capitalize(lower){
    return lower.charAt(0).toUpperCase() + lower.substr(1);
}
