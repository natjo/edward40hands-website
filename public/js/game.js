class Stopwatch {
    // showXYZ must always direct to a span or label or whatever can show text, results must be a <ul>
    constructor(showHours, showMinutes, showSeconds, showMilliseconds, results) {
        this.showHours = showHours;
        this.showMinutes = showMinutes;
        this.showSeconds = showSeconds;
        this.showMilliseconds = showMilliseconds;
        this.results = results;
        this.running = false;
        this.laps = [];
        this.reset();
        this.print(this.times);
    }

    getTotalSeconds(){
        return this.times[0] * 3600 + this.times[1] * 60 + this.times[2];
    }

    isRunning(){
        return this.running;
    }

    start() {
        if (!this.time) this.time = performance.now();
        if (!this.running) {
            this.running = true;
            requestAnimationFrame(this.step.bind(this));
        }
    }

    stop() {
        this.running = false;
        this.time = null;
    }

    reset() {
        this.times = [ 0, 0, 0, 0 ];
        this.running = false;
        this.print(this.time);
    }

    restart() {
        if (!this.time) this.time = performance.now();
        if (!this.running) {
            this.running = true;
            requestAnimationFrame(this.step.bind(this));
        }
        this.reset();
    }

    clear() {
        clearChildren(this.results);
    }

    step(timestamp) {
        if (!this.running) return;
        this.calculate(timestamp);
        this.time = timestamp;
        this.print();
        requestAnimationFrame(this.step.bind(this));
    }

    calculate(timestamp) {
        var diff = timestamp - this.time;
        // Hundredths of a second are 100 ms
        this.times[3] += diff / 10;
        // Seconds are 100 hundredths of a second
        if (this.times[3] >= 100) {
            this.times[2] += 1;
            this.times[3] -= 100;
        }
        // Minutes are 60 seconds
        if (this.times[2] >= 60) {
            this.times[1] += 1;
            this.times[2] -= 60;
        }
        // Hours are 60 seconds
        if (this.times[1] >= 60) {
            this.times[0] += 1;
            this.times[1] -= 60;
        }
    }

    print() {
        this.showHours.text(`${pad0(this.times[0], 2)}`);
        this.showMinutes.text(`:${pad0(this.times[1], 2)}`);
        this.showSeconds.text(`:${pad0(this.times[2], 2)}`);
        this.showMilliseconds.text(`:${pad0(Math.floor(this.times[3]*10), 3)}`);
    }

    getTimeString() {
        return `${pad0(this.times[0], 2)}:${pad0(this.times[1], 2)}:${pad0(this.times[2], 2)}`;
    }
}

function pad0(value, count) {
    var result = value.toString();
    for (; result.length < count;){
        result = '0' + result;
    }
    return result;
}

function clearChildren(node) {
    while (node.lastChild)
        node.removeChild(node.lastChild);
}

var GLOB = (function() {

    ////////////////////////////// Variables
    var stopwatch = new Stopwatch($('#hours'), $('#minutes'), $('#seconds'), $('#milliseconds'), $('.results'));
    var extraBeers = {};
    var times = {};

    ////////////////////////////// Public functions
    var publicObject = {};


    publicObject['extraBeer'] = function(selectIndex){
        var name = $(`#selectPlayer${selectIndex}`).val();
        if(name == 'none'){
            alert(`Du hast noch keinen Spieler ausgewählt.`);
            return;
        }else if(!stopwatch.isRunning()){
            alert(`Das Spiel läuft noch nicht. Vor Spielbeginn gibt es noch kein Strafbier.`);
            return;
        }else{
            extraBeers[name] += 1;
            $(`#numExtraBeer${selectIndex}`).text(extraBeers[name])
        }
    }

    publicObject['addPlayer'] = function(){
        // Check if game is already running
        if(stopwatch.isRunning()) return;
        // generate new view
        let playerNum = $('#players > div').length + 1;
        // $('#players').append(generatePlayerView(numPlayers));
        $(generatePlayerView(playerNum)).insertBefore('#addPlayerButton');

        // Add onChange listener
        $(`#selectPlayer${playerNum}`).on('change', function() {
            // Check if name is already in use
            var newName = this.value;
            if(newName == 'none') return;
            if(countInArray(getAllNames(),newName) > 1){
                alert(`${capitalize(newName)} ist in der Runde schon dabei! Wähle jemand anderen.`);
                $(`#selectPlayer${playerNum}`).val($(`#selectPlayer${playerNum} option:first`).val());
            }else{
                // Add player
                extraBeers[newName] = 0;
                times[newName] = 0;
                $(`#numExtraBeer${playerNum}`).text(extraBeers[name]);
                generateTitle();
            }
        });
    }

    publicObject['start'] = function(){
        // Check if all players have names set
        for(var i=0;i<=$('#players > div').length;i++){
            if($(`#selectPlayer${i}`).val() == 'none'){
                alert(`Einer oder mehrere Spieler sind noch nicht ausgewählt.`);
                return;
            }
        }

        // Disable select player fields
        for(var i=0;i<=$('#players > div').length;i++){
            $(`#selectPlayer${i}`).prop('disabled', true);
        }

        stopwatch.start();
    }

    publicObject['stop'] = function(selectIndex){
        var name = $(`#selectPlayer${selectIndex}`).val();
        if(!stopwatch.isRunning()){
            return;
        }else{
            // Valid stop
            times[name] = stopwatch.getTotalSeconds();
            $(`#timePlayer${selectIndex}`).text(stopwatch.getTimeString());
            $(`#stopButtonPlayer${selectIndex}`).prop('disabled', true);

            // Check if all player stopped. If so, finish the round
            finishGame();
        }
    }

    publicObject['reset'] = function(){
        resetPage();
    }

    ////////////////////////////// Private
    function resetPage(){
        stopwatch.reset();
        // Reset all players
        for(var i=1;i<=$('#players > div').length;i++){
            var name = $(`#selectPlayer${i}`).val();

            // Enable select player and stop button fields
            $(`#selectPlayer${i}`).prop('disabled', false);
            $(`#stopButtonPlayer${i}`).prop('disabled', false);

            // Undo all extraBeers
            extraBeers[name] = 0;
            $(`#numExtraBeer${i}`).text(0);

            // Reset time
            times[name] = 0;
            $(`#timePlayer${i}`).text("Drücke Stop wenn dein Bier alle ist!");
        }
    }
    // Check if all player stopped. If so, finish the round
    function finishGame(){
            var allDone = true;
            getAllNames().forEach((name) =>{
                if(times[name] == 0) allDone = false;
            });
            if(allDone){
                stopwatch.stop();
                if(confirm("Das Spiel ist vorbei. Soll es in die offizielle Wertung eingehen?")){
                    uploadGames();
                }
                resetPage();
            }

    }
    function uploadGames(){
        var names = getAllNames();

        for(var i = 0; i < names.length; i++){
            for(var j = i + 1; j < names.length; j++){
                name1 = names[i];
                name2 = names[j];
                console.log(name1 + ", " + name2 + ", " + times[name1] + ", " + times[name2] + ", " + extraBeers[name1] + ", " + extraBeers[name2]);
            $.post('uploadGame', {
                    "name1": name1,
                    "name2" : name2,
                    "time1" : times[name1],
                    "time2" : times[name2],
                    "extraBeers1" : extraBeers[name1],
                    "extraBeers2" : extraBeers[name2], },
                function(returnedData){
                     console.log(returnedData);
            });
            }
        }
    }

    function getAllNames(){
        var names = [];
        for(var i=1;i<=$('#players > div').length;i++){
            names.push($(`#selectPlayer${i}`).val());
        }
        return names;
    }

    function generateTitle(){
        // Get all names, filter out none
        var names = getAllNames().filter((name) => name != 'none');
        var titleStr = '';
        names.forEach((name) => {
            titleStr += (capitalize(name) + ' vs ');
        });

        if(names.length > 1) titleStr = titleStr.slice(0, -4);
        $('#matchTitle').text(titleStr);
    }

    function countInArray(array, what) {
        return array.filter(item => item == what).length;
    }

    function capitalize(lower){
        return lower.charAt(0).toUpperCase() + lower.substr(1);
    }

    function generatePlayerView(numPlayer){
        return `
        <!-- player ${numPlayer} -->
        <div class="d-flex flex-column border-right border-bottom p-3 m-3 bg-light">
            <h3 class="text-center"> ${numPlayer}. Spieler </h3>

            <select id="selectPlayer${numPlayer}" class="custom-select my-3">
                <option value="none">Wähle einen Spieler</option>
                <option value="benni">Benni</option>
                <option value="bene">Bene</option>
                <option value="jonas">Jonas</option>
                <option value="matthias">Matthias</option>
                <option value="tobi">Tobi</option>
            </select>

            <div class="d-flex flex-row my-3">
                <span class="font-weight-bold mr-2">Zeit:</span>
                <span id="timePlayer${numPlayer}"> Drücke Stop wenn dein Bier alle ist!</span>
            </div>

            <div class="d-flex flex-row my-3">
                <span class="font-weight-bold mr-2">Anzahl Strafbiere:</span>
                <span id="numExtraBeer${numPlayer}">0</span>
            </div>

            <div class="d-flex flex-row justify-content-around my-3">
                <button id="stopButtonPlayer${numPlayer}" class="btn btn-danger btn-lg" onClick="GLOB.stop(${numPlayer});">Stop</button>
                <a href="#" class="btn btn-warning btn-lg" onClick='GLOB.extraBeer(${numPlayer});'>Strafbier</a>
            </div>
        </div>
        `
    }

    return publicObject;

})();

document.addEventListener('DOMContentLoaded', function() {
    GLOB.addPlayer();
    GLOB.addPlayer();
}, false);
