// Load data during first call
document.addEventListener('DOMContentLoaded', function() {
    onLoad();
}, false);

function onLoad(){
    loadGames();
}

function generateRowHTML(unixDate, dateStr, name1, name2, winner, timeStr1, timeStr2, extraBeers1, extraBeers2){
    let imgSize = 96;

    let buttonImg = `<img id="addPlayerButton" class="btn-light icon-minus border-right border-bottom" src="/icons/svg/minus.svg" alt="Minus symbol" onClick="removeGame(${unixDate});">`;
    let tableRow = `<tr class="text-center"><td scope="row">${dateStr}</td><td>${name1} vs ${name2}</td><td>${winner}</td><td>${timeStr1} - ${timeStr2}</td><td>${extraBeers1} - ${extraBeers2}</td><td>${buttonImg}</tr></tr>`
    return tableRow;
}

function loadGames(){
    $.get('/getAllGames', function(data){
        // First sort data, first by date
        data.sort(function(a,b) {
            return a.unixDate > b.unixDate;
        });

        data.forEach(doc => {
            // Find the winner
            let winner = doc.name1;
            if(doc.time1 > doc.time2){
                winner = doc.name2;
            }
            $('#tableGameHistory tbody:last-child').append(
                generateRowHTML(
                    doc.unixDate,
                    unixDate2str(doc.unixDate),
                    capitalize(doc.name1),
                    capitalize(doc.name2),
                    capitalize(winner),
                    sec2str(doc.time1),
                    sec2str(doc.time2),
                    doc.extraBeers1,
                    doc.extraBeers2)
            );
        });
    });
}

function removeGame(unixDate){
    if(confirm(`Sicher, dass du das Spiel vom ${unixDate2str(unixDate)} löschen willst?`)){
        $.post('fakeDeleteGame', { "unixDate" : unixDate },
            function(returnedData){
                 console.log(returnedData);
        });
    }
}

function unixDate2str(unixDate){
    // Create a new JavaScript Date object based on the timestamp
    // multiplied by 1000 so that the argument is in milliseconds, not seconds.
    var date = new Date(unixDate*1000);
    var months = ['Jan','Feb','Mar','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'];
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    // var month = date.getMonth() + 1;
    var day = date.getDate();
    // var hours = date.getHours();
    // var minutes = "0" + date.getMinutes();
    // var seconds = "0" + date.getSeconds();
    // return hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    return day + '. ' + month + " '" + year.toString().substr(-2);
}

function sec2str(seconds) {
    var sec_num = parseInt(seconds, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours+':'+minutes+':'+seconds;
};

function capitalize(lower){
    return lower.charAt(0).toUpperCase() + lower.substr(1);
}
