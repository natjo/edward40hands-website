# edward40hands-website

## Requesits
- active mongodb service
- npm

Create a symbolic link for the node service to run the server:
`ln -s src/node /etc/init.d/node`

## How to Start
After cloning this repo, change into the main folder and run:
- `npm install` 
- `npm run initialize`  (This will create all users in the database)
- `npm start`